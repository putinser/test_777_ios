//
//  Messages.swift
//  TestApp
//
//  Created by Balina on 2/18/20.
//  Copyright © 2020 Kudin Sergey. All rights reserved.
//

import Foundation
import SwiftyJSON

class Message {
    
    var id: String?
    var time: String?
    var name: String?
    var image: String?
    
    init(json: JSON) {
        self.id = json["Id"].stringValue
        self.time =  json["Time"].stringValue
        self.name = json["Name"].stringValue
        self.image = json["Image"].stringValue
    }
    
    init() {
    }
}
