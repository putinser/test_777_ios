//
//  DataManager.swift
//  TestApp
//
//  Created by Balina on 2/18/20.
//  Copyright © 2020 Kudin Sergey. All rights reserved.
//

import Foundation
import SwiftyJSON

class DataManager: NSObject {

static let shared = DataManager()

    typealias Completion = (Bool, JSON, String) -> ()

    func getDataFromJson(completion: @escaping Completion) {
        
        if let path = Bundle.main.path(forResource: "test", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
                let jsonObj = try JSON(data: data)
                print("jsonData:\(jsonObj)")
                completion(true, jsonObj, "")
            } catch let error {
                print("parse error: \(error.localizedDescription)")
                completion(false, [], "Unknown error")
            }
        } else {
            print("Invalid filename/path.")
            completion(false, [], "Unknown error")
        }
        
    }

}
