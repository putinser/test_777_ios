//
//  ListViewController.swift
//  TestApp
//
//  Created by Balina on 2/18/20.
//  Copyright © 2020 Kudin Sergey. All rights reserved.
//

import UIKit
import SDWebImage

class MasterListCell: UITableViewCell {

    let phoneNumber: String? = "нет телефона"
    var noMyMaster = false
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        phoneNumberLabel.text = phoneNumber
        userImage.layer.cornerRadius = 6
        userImage.layer.masksToBounds = true

    }
}

class ListViewController: UIViewController {
    
    static unowned var storyboardInstance: ListViewController {
        return UIStoryboard(name: String(describing: self), bundle: nil).instantiateInitialViewController() as! ListViewController
    }
    
    //MARK: - properties
    
    private var messages: [Message] = []
    
    private var cellIdentifier = "MessageCell"
    
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: - life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    
    //MARK: - methods
    
    private func initialSetup() {
   
        tableView.delegate = self
        tableView.dataSource = self
        tableView?.tableFooterView = UIView()
        
        setupNavigationItems()

        getMasters(completionSuccess: { (mastersArray) in
            self.messages = mastersArray
            self.tableView.reloadData()
        }) { (message) in
//            self.showErrorAlert(error: message)
        }
    }
    
    func getMasters(completionSuccess: @escaping (_ masters: [Message]) -> (), completionError: @escaping (_ message: String) -> ()) {
        
        DataManager.shared.getDataFromJson { (success, json, message) in
            if success {
                var messages: [Message] = []
                let arrayValue = json.arrayValue
                for item in arrayValue {
                    let newMessage = Message(json: item)
                    messages.append(newMessage)
                }
                completionSuccess(messages)
            } else {
                print(message)
                completionError(message)
            }
        }
    }
    
    
    private func setupNavigationItems() {
        navigationItem.title = "Сообщения"
        
    }
    
    //MARK: - actions
    
    @objc private func actionBack() {
        navigationController?.popViewController(animated: true)
    }
    
    //MARK: - IBActions
    
}

//MARK: - extensions

extension ListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! MasterListCell
        
        let message = messages[indexPath.item]
        
        if let url = message.image {
            cell.userImage.sd_setImage(with: URL(string: url), placeholderImage: #imageLiteral(resourceName: "ic_b_user"), options: [], completed: nil)
        } else {
            cell.userImage.image = #imageLiteral(resourceName: "ic_b_user")
        }
        

        cell.nameLabel.text = message.name
        cell.phoneNumberLabel.text = message.time
        
        cell.selectionStyle = .none
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let action = UIContextualAction(style: .normal, title: title,
                                        handler: { (action, view, completionHandler) in
                                            
                                            self.messages.remove(at: indexPath.row)
                                            self.tableView.reloadData()
                                            
                                            completionHandler(true)
        })
        
        action.image = #imageLiteral(resourceName: "bt_h_clear")
        action.backgroundColor = .red
        let configuration = UISwipeActionsConfiguration(actions: [action])
        return configuration
    }

}

