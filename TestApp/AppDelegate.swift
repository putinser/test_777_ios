//
//  AppDelegate.swift
//  TestApp
//
//  Created by Balina on 2/18/20.
//  Copyright © 2020 Kudin Sergey. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        let controlerVC = ListViewController.storyboardInstance
        let controlerNavVC = UINavigationController(rootViewController: controlerVC)
        self.window?.rootViewController = controlerNavVC
        
        return true
    }

}

